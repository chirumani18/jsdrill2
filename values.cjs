function values(testObject) {
    const value_list = [];
    if (typeof (testObject) === "object" && testObject.length != 0) {
        for (let key in testObject) {
            if (typeof testObject[key] != 'function') {
                value_list.push(testObject[key]);
            }
        }
        return value_list;
    }
    return [];
}
module.exports = values;