function invert(testObject) {
    if (typeof (testObject) === "object" && testObject.length != 0) {
        let invert_obj = {};
        for (let key in testObject) {
            invert_obj[testObject[key]] = key;
        }
        return invert_obj;
    }
    return {};
}
module.exports = invert;