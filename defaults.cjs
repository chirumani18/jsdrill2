function defaults(testObject, defaultProps) {
    for (let key in defaultProps) {
        if (typeof testObject[key] == 'undefined') {
            testObject[key] = defaultProps[key];
        }
    }
    return testObject;
}

module.exports = defaults;
