function mapObject(testObject, cb) {
    if (typeof (testObject) === "object" && testObject.length != 0) {
        const new_obj = {};
        for (let key in testObject) {
            new_obj[key] = cb(testObject[key]);
        }
        return new_obj;
    }
    return {};
}

module.exports = mapObject;
