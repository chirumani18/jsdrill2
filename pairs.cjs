function pairs(testObject) {
    if (typeof (testObject) === "object" && testObject.length != 0) {
        const pairs_list = [];
        for (let key in testObject) {
            const append_list = [];
            append_list.push(key);
            append_list.push(testObject[key]);
            pairs_list.push(append_list);
        }
        return pairs_list;
    }
    return [];
}

module.exports = pairs;