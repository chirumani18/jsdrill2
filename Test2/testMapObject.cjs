const mapObject = require('../mapObject.cjs');
const testObject = require('../testObject.cjs');

function add_20(value) {
    if (typeof value != 'number') {
        return value
    }
    return value + 20;
}

console.log(mapObject(testObject, add_20));